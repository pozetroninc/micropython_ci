import os
from pathlib import Path

__template__ = """
{board}:
  <<: *stm32
{board}_mboot:
  <<: *stm32
  allow_failure: true
"""

stm32_yaml = Path(__file__).parent / ".gitlab-ci.yml"
ci_script = stm32_yaml.read_text()

stm32_boards_dir = Path(__file__).parent / ".." / ".." / "micropython" / "ports" / "stm32" / "boards"
stm32_boards = [p.name for p in stm32_boards_dir.iterdir() if p.is_dir()]

ci_script_divider = '#######################################'
ci_script_parts = ci_script.split(ci_script_divider)
ci_script = ci_script_divider.join(ci_script_parts[:-1])

updated = []

for board in sorted(stm32_boards):
    definition = __template__.format(board=board)

    if definition not in ci_script_parts[-1]:
        updated.append(board)
    
    ci_script += definition

if updated:
    stm32_yaml.write_text(ci_script)
    os.system("git add %s" % stm32_yaml)
    os.system('git commit -m "Added stm32 boards: %s"' % ",".join(updated))
